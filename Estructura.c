#include <stdio.h>
#include <stdlib.h>

typedef struct
{
	int base;
	int altura;
	int resultado;
}figura;

int main()
{
	figura *area = (figura *) malloc(sizeof(figura));
	printf("Ingresa base: ");
	scanf("%d", &area->base);

	printf("Ingresa altura: ");
	scanf("%d", &area->altura);
	area->resultado = (area->base) * (area->altura);
	
	printf("Resultado: %d\n", area->resultado);
}
